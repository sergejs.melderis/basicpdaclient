package com.sinch.melderis.pdaclient;

import com.sinch.ai.phishingdetection.v1alpha1.AnalysisRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UrlController {

    @Autowired
    KafkaTemplate<String, byte[]> kafkaTemplate;

    @PostMapping("/url")
    public HttpEntity<String> check(@RequestBody final String url) {
        System.out.println("Received url " + url);
        var request = AnalysisRequest.newBuilder().setUrl(url).setAccountReference("test").setMessageId("123").build();
        System.out.println("publishing " + request);
        kafkaTemplate.send("control.data.science.phishing.detection.request", String.valueOf(System.nanoTime()), request.toByteArray());
        return ResponseEntity.ok().build();
    }

}
