package com.sinch.melderis.pdaclient;

import com.google.protobuf.InvalidProtocolBufferException;
import com.sinch.ai.phishingdetection.v1alpha1.PhishingURL;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class PhishingAnalysisKafkaListener {


    @KafkaListener(
            groupId = "mygroup",
            topics = "control.data.science.phishing.detection.threat.local")
    public void databasePersistenceListener(final ConsumerRecord<String, byte[]> record) {

        System.out.println("phishing analysis database listener - received phishing url");
        try {
            PhishingURL phishingURL = PhishingURL.parseFrom(record.value());
            System.out.println(phishingURL);
        } catch (Exception ex) {
            log.error("", ex);
        }
    }
}
