package com.sinch.melderis.pdaclient;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.SerializationUtils;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Component
public class KafkaDefaultPublisher {

    private final KafkaTemplate<String, byte[]> kafkaTemplate;

    public <T> void publish(final T data, final String topicName) {
        String key = UUID.randomUUID().toString();
        byte[] body = SerializationUtils.serialize(data);
        log.info("KafkaDefaultPublisher - publishing information with key: {}", key);
        kafkaTemplate.send(topicName, key, body)
                .addCallback(new ListenableFutureCallback<>() {
                    @Override
                    public void onSuccess(final SendResult<String, byte[]> sendResult) {
                        long offset = sendResult.getRecordMetadata().offset();
                        log.debug("information published. topic={}, key={}, offset={}", topicName, key, offset);
                    }

                    @Override
                    public void onFailure(@NotNull final Throwable throwable) {
                        log.error(String.format("unable to publish information. topic=%s, key=%s", topicName, key), throwable);
                    }
                });
    }
}
